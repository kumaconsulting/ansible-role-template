# ansible-role-template


A [cookiecutter](https://github.com/audreyr/cookiecutter) template for Ansible roles with Molecule testing framwork. 

Cookiecutter provides a native template interface, so developers can
provide their own templates.


## Usage


```bash
python3 -m pip install cookiecutter
cookiecutter https://gitlab.com/kumaconsulting/ansible-role-template.git
```

## Autheur
©2022 Kuma-Consulting - Jérémie CUADRADO (redbeard28)

## Licence
Tous droits réservés à Jérémie CUADRADO

Toutes les marques citées appartiennent à leurs propriétaires; marques de tiers, noms de produits, noms commerciaux, dénominations sociales et noms de société cités sont des marques commerciales de leurs propriétaires respectifs ou des marques déposées d'autres sociétés et ne sont utilisés que pour des fins d'illustration et au bénéfice de leur propriétaire, sans que cela n'implique d'aucune manière une violation du droit d'auteur. 

